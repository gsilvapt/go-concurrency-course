package main

import (
	"fmt"
	"sync"
)

// var (
// 	msg string
// 	wg  sync.WaitGroup
// )

// func updateMsg(s string, m *sync.Mutex) {
// 	defer wg.Done()
// 	m.Lock()
// 	msg = s
// 	m.Unlock()
// }

// func main() {
// 	msg = "Hello, world!"

// 	var mutex sync.Mutex

// 	wg.Add(2)
// 	go updateMsg("Hello, Universe!", &mutex)
// 	go updateMsg("Hello, Cosmos!", &mutex)
// 	wg.Wait()

// 	fmt.Println(msg)
// }

// COMPLEX EXAMPLE

var (
	wg sync.WaitGroup
)

type Income struct {
	Source string
	Amount int
}

func main() {
	// variables bank balance
	// print out starting values
	// define weekly revenue
	// loop through 52 weeks and print out how much is made; keep running total
	// print out final balance
	var (
		balance int
		m       sync.Mutex
	)

	fmt.Printf("Initial account balance: $%d.00\n", balance)
	incomes := []Income{
		{Source: "Main job", Amount: 500},
		{Source: "Gifts", Amount: 10},
		{Source: "Part-time job", Amount: 50},
		{Source: "Investments", Amount: 100},
	}

	wg.Add(len(incomes))
	for i, income := range incomes {
		go func(i int, inc Income) {
			defer wg.Done()
			for week := 1; week <= 52; week++ {
				m.Lock()
				temp := balance
				temp += inc.Amount
				balance = temp
				m.Unlock()
				fmt.Printf("On week %d, you earned $%d.00 from %s.\n", week, inc.Amount, inc.Source)
			}
		}(i, income)
	}
	wg.Wait()

	fmt.Printf("Final balance: $%d.00.\n", balance)
}
