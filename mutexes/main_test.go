package main

import (
	"io"
	"os"
	"strings"
	"testing"
)

// func Test_updateMsg(t *testing.T) {
// 	msg = "Hello, world!"
// 	var m sync.Mutex

// 	wg.Add(1)
// 	go updateMsg("Goodbye, cruel world!", &m)
// 	wg.Wait()

// 	if msg != "Goodbye, cruel world!" {
// 		t.Errorf("expectec Goobye..., got %s", msg)
// 	}
// }

func Test_Main(t *testing.T) {
	stdout := os.Stdout
	r, w, _ := os.Pipe()

	os.Stdout = w

	main()
	_ = w.Close()
	result, _ := io.ReadAll(r)
	output := string(result)

	os.Stdout = stdout

	if !strings.Contains(output, "$34320.00") {
		t.Errorf("Wrong balance returned; expected 34320, got %s \n", output)
	}
}
