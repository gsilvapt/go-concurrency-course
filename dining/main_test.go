package main

import (
	"testing"
	"time"
)

func Test_dine(t *testing.T) {
	eatTime = 0 * time.Second
	sleepTime = 0 * time.Second
	thinkTime = 0 * time.Second

	for i := 0; i < 10; i++ {
		orderFinished = []string{}                   // reset package-level variable
		dine()                                       // fires off necessary go routines
		if len(orderFinished) != len(philosophers) { // package-level variable
			t.Errorf("expected slice to have the same length of philosophers slice defined in main.go. Got %d", len(orderFinished))
		}
	}
}

func Test_dineWithVaryingDelays(t *testing.T) {
	var tests = []struct {
		name  string
		delay time.Duration
	}{
		{"zero delay", 0 * time.Second},
		{"quarter second delay", 250 * time.Millisecond},
		{"half second delay", 500 * time.Millisecond},
	}

	for _, tc := range tests {
		orderFinished = []string{} // reset package-level variable
		eatTime = tc.delay
		sleepTime = tc.delay
		thinkTime = tc.delay
		dine()                                       // fires off necessary go routines
		if len(orderFinished) != len(philosophers) { // package-level variable
			t.Errorf("expected slice to have the same length of philosophers slice defined in main.go. Got %d", len(orderFinished))
		}
	}
}
