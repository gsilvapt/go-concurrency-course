package main

import (
	"fmt"
	"sync"
	"time"
)

// The Dining Philosophers problem is well known in computer science.
// Five philosophers, live in a house where the table is laid for them; each philosopher has their own place at the table.
// The problem is their dish is special and requires to be eaten with two forks. There are two forks next to each plate.
// As a consequence, this means that no two neighbours may be eating simultanouesly, since there are 5 philosophers and 5
// forks.

// Dijkstra's solution

// Philosopher is a struct that stores information about a Philospher.
type Philosopher struct {
	name      string
	rightfork int
	leftfork  int
}

var philosophers = []Philosopher{
	{name: "Plato", leftfork: 4, rightfork: 0},
	{name: "Socrates", leftfork: 0, rightfork: 1},
	{name: "Aristotle", leftfork: 1, rightfork: 2},
	{name: "Pascal", leftfork: 2, rightfork: 3},
	{name: "Locke", leftfork: 3, rightfork: 4},
}

var (
	hunger        = 3               //how many times they eat before done
	eatTime       = 1 * time.Second // time it takes them to eat each time.
	thinkTime     = 3 * time.Second // after eating, they think
	sleepTime     = 1 * time.Second // time to sleep when printing things out
	orderFinished []string          // contains the order philosophers ended dining and left the table
	orderMut      sync.Mutex        // mutex to lock slice orderFinished
)

func main() {
	// print out welcome message
	fmt.Println("Welcome to the Philosophers Dilemma solution")
	fmt.Println("--------------------------------------------")
	fmt.Println("The table is empty.")

	// start the meal
	dine()
	// print out finished message
	time.Sleep(sleepTime)
	fmt.Println("The table is empty.")
}

func dine() {
	// variable override for dev purposes
	// eatTime = 0 * time.Second
	// sleepTime = 0 * time.Second
	// thinkTime = 0 * time.Second
	wg := &sync.WaitGroup{}
	wg.Add(len(philosophers))

	seated := &sync.WaitGroup{}
	seated.Add(len(philosophers))

	// forks maps all 5 forks
	var forks = make(map[int]*sync.Mutex)
	for i := 0; i < len(philosophers); i++ {
		forks[i] = &sync.Mutex{}
	}

	// start the meal
	for i := 0; i < len(philosophers); i++ {
		// start go routine for the current philosopher
		go diningProblem(&philosophers[i], wg, forks, seated)
	}

	wg.Wait()
	fmt.Printf("the order philosopheres ended dining is %v\n", orderFinished)
}

func diningProblem(p *Philosopher, wg *sync.WaitGroup, forks map[int]*sync.Mutex, seated *sync.WaitGroup) {
	defer wg.Done()

	// seat philosopher at the table
	fmt.Printf("%s is seated at the table.\n", p.name)
	seated.Done()
	seated.Wait()

	// eat 3 times
	for i := hunger; i > 0; i-- {
		if p.leftfork > p.rightfork {
			// to ensure the philosopher can pick both forks, we ensure the pick the lower numbered first.
			// check if philosophers can pick up the forks
			forks[p.rightfork].Lock()
			fmt.Printf("\t%s takes the right fork.\n", p.name)
			forks[p.leftfork].Lock()
			fmt.Printf("\t%s takes the left fork.\n", p.name)
		} else {
			// to ensure the philosopher can pick both forks, we ensure the pick the lower numbered first.
			// check if philosophers can pick up the forks
			forks[p.leftfork].Lock()
			fmt.Printf("\t%s takes the left fork.\n", p.name)
			forks[p.rightfork].Lock()
			fmt.Printf("\t%s takes the right fork.\n", p.name)
		}

		fmt.Printf("\t%s has both forks and is eating.\n", p.name)
		time.Sleep(eatTime)

		fmt.Printf("\t%s is thinking.\n", p.name)
		time.Sleep(thinkTime)

		forks[p.leftfork].Unlock()
		forks[p.rightfork].Unlock()

		fmt.Printf("\t%s put down the forks.\n", p.name)
	}

	fmt.Println(p.name, "is satisfied.")
	fmt.Println(p.name, "left the table")
	orderMut.Lock()
	orderFinished = append(orderFinished, p.name)
	orderMut.Unlock()
}
