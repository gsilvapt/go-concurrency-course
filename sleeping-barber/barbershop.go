package main

import (
	"time"

	"github.com/fatih/color"
)

type BarberShop struct {
	ShopCap          int
	HaircutDuration  time.Duration
	NumBarbers       int
	BarbersDoneChan  chan bool
	ClientsQueueChan chan string
	Open             bool
}

func (b *BarberShop) AddBarber(name string) {
	b.NumBarbers += 1

	go func() {
		isSleeping := false
		color.Yellow("%s goes to the waiting room to look for clients.", name)

		for {
			if len(b.ClientsQueueChan) == 0 {
				color.Yellow("There is nothing to do, so %s takes a nap", name)
				isSleeping = true
			}

			client, shopOpen := <-b.ClientsQueueChan //shopOpen will be false once the ClientsQueueChan is closed
			if shopOpen {
				if isSleeping {
					color.Yellow("%s wakes %s up.", client, name)
					isSleeping = false
				}
				b.cutHair(name, client)
			} else {
				// shop is closed so,
				b.sendBarberHome(name)
				return
			}
		}
	}()
}

func (b *BarberShop) cutHair(barberName, clientName string) {
	color.Green("%s is cutting %s's hair.", barberName, clientName)
	time.Sleep(b.HaircutDuration)
	color.Green("%s is finished %s's hair.", barberName, clientName)
}

func (b *BarberShop) sendBarberHome(barberName string) {
	color.Cyan("%s is going home", barberName)
	b.BarbersDoneChan <- true
}

func (b *BarberShop) CloseShop() {
	color.Cyan("Closing shop for the day.")
	close(b.ClientsQueueChan)
	b.Open = false

	for a := 1; a <= b.NumBarbers; a++ {
		<-b.BarbersDoneChan // blocks until all barbers complete the cut
	}
	close(b.BarbersDoneChan)

	color.Green("-------------------------------")
	color.Green("Shop is now closed for the day.")
}

func (b *BarberShop) AddClient(name string) {
	color.Green("*** %s arrives", name)

	if b.Open {
		select {
		case b.ClientsQueueChan <- name:
			color.Yellow("%s takes a seat in the waiting room", name)
		default:
			color.Red("The waiting room is full, so %s leaves.", name)
		}
	} else {
		color.Red("Shop is closed, so %s leaves", name)
	}
}
