// Simple demo to solve the Sleeping Barber Dilemma, a classical computer science problem demonstrating the complexities
// that arise when managing multiple operating system processes.
// We have:
// Limited number of barbers, finite number of seats, fixed length of time the barbershop is open, and clients arriving
// at roughtly regular intervals.
// When a barber has nothing to do, s/he checks the waiting room for new clients. If there is someone, the haircut takes
// place. Otherwise, the barber goes to sleep until new clients arrive.
package main

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/fatih/color"
)

// const ( defining as package wide variables can be useful for testing because consts cannot be redefined.
var (
	seatingCap    = 10
	arrivalRateMs = 100
	cutDuration   = 1000 * time.Millisecond
	timeOpen      = 10 * time.Second
)

func main() {
	// seed our rng
	rand.Seed(time.Now().UnixNano())

	// print welcome message
	color.Yellow("The Sleeping Barber Problem")
	color.Yellow("---------------------------")

	// create channels if we need any
	cliChan := make(chan string, seatingCap) // allows more than 1 client but no more
	doneChan := make(chan bool)              // when everything is done

	// create the barbershop
	shop := &BarberShop{
		ShopCap:          seatingCap,
		HaircutDuration:  cutDuration,
		NumBarbers:       0,
		BarbersDoneChan:  doneChan,
		ClientsQueueChan: cliChan,
		Open:             true,
	}

	color.Green("The shop is open for business")

	// add barbers
	shop.AddBarber("Frank")
	shop.AddBarber("Brank")
	shop.AddBarber("Milton")
	shop.AddBarber("Janette")
	shop.AddBarber("Kelly")
	shop.AddBarber("Susan")
	shop.AddBarber("John")
	shop.AddBarber("Brisk")

	// start the barbershop as a goroutine
	shopClosing, closed := make(chan bool), make(chan bool)

	go func() {
		<-time.After(timeOpen) // this blocks until timeOpen is gone
		shopClosing <- true
		shop.CloseShop()
		closed <- true
	}()

	// add clients
	i := 1
	go func() {
		for {
			// get random number with average arrival date
			randMillSec := rand.Int() % (2 * arrivalRateMs)
			select {
			case <-shopClosing:
				return
			case <-time.After(time.Duration(randMillSec) * time.Millisecond):
				shop.AddClient(fmt.Sprintf("Client #%d", i))
				i += 1
			}
		}
	}()
	// block (keep the main thread running) until the barbershop is closed
	<-closed
}
