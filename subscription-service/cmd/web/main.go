package main

import (
	"database/sql"
	"encoding/gob"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"subscription-service/data"
	"sync"
	"syscall"
	"time"

	"github.com/alexedwards/scs/redisstore"
	"github.com/alexedwards/scs/v2"
	"github.com/gomodule/redigo/redis"
	_ "github.com/jackc/pgconn"
	_ "github.com/jackc/pgx/v4"
	_ "github.com/jackc/pgx/v4/stdlib"
)

const webPort = "8080"

func main() {
	// connect to database
	db := initDB()

	// create sessions
	session := initSession()

	// create loggers
	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stdout, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	// create channels
	// create waitgroup
	wg := sync.WaitGroup{}

	// set up the app config
	appCfg := Config{
		Session:       session,
		DB:            db,
		wg:            &wg,
		InfoLog:       infoLog,
		ErrorLog:      errorLog,
		Models:        data.New(db),
		ErrorChan:     make(chan error),
		ErrorChanDone: make(chan bool),
	}

	// set up mail
	appCfg.Mailer = appCfg.CreateMail()
	go appCfg.ListenForMail()

	go appCfg.listenForErrors()

	// listen for signals
	go appCfg.ListenForShutdown()
	// listen for web connections
	appCfg.Serve()
}

func (c *Config) listenForErrors() {
	for {
		select {
		case err := <-c.ErrorChan:
			// TODO: Notify something like slack, email, wtv?
			c.ErrorLog.Println(err)
		case <-c.ErrorChanDone:
			return
		}
	}
}

func (c *Config) Serve() {
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", webPort),
		Handler: c.routes(),
	}

	c.InfoLog.Println("starting web server...")
	if err := srv.ListenAndServe(); err != nil {
		log.Panic(err)
	}

}

func initDB() *sql.DB {
	conn := getDBConn()
	if conn == nil {
		log.Panic("could not connect to database")
	}

	return conn
}

func getDBConn() *sql.DB {
	counts := 0
	dsn := os.Getenv("DSN")

	for {
		connection, err := openDB(dsn)
		if err != nil {
			log.Println("postgres not yet ready...")
		} else {
			log.Println("connected to postgres")
			return connection
		}

		if counts > 10 {
			return nil
		}

		log.Println("retrying in a few seconds")
		time.Sleep(1 * time.Second)
		counts++
		continue
	}
}

func openDB(dsn string) (*sql.DB, error) {
	db, err := sql.Open("pgx", dsn)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, nil
}

func initSession() *scs.SessionManager {
	gob.Register(data.User{})
	// gob.Register(data.Plan{})

	session := scs.New()
	session.Store = redisstore.New(getRedisConn())
	session.Lifetime = 24 * time.Hour
	session.Cookie.Persist = true
	session.Cookie.SameSite = http.SameSiteLaxMode
	session.Cookie.Secure = true

	return session
}

func getRedisConn() *redis.Pool {
	redisPool := &redis.Pool{
		MaxIdle: 10,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", os.Getenv("REDIS"))
		},
	}

	return redisPool
}

func (c *Config) ListenForShutdown() {
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit // block until something gets to quit
	c.shutdown()
	os.Exit(0)
}

func (c *Config) shutdown() {
	// perform any needed cleanup tasks
	c.InfoLog.Println("would run cleanup tasks...")
	c.wg.Wait() // block until wg is empty
	c.Mailer.DoneChan <- true
	c.ErrorChanDone <- true

	c.InfoLog.Println("closing channels and shutting down app...")
	close(c.Mailer.MailerChan)
	close(c.Mailer.ErrorChan)
	close(c.Mailer.DoneChan)
	close(c.ErrorChan)
	close(c.ErrorChanDone)
}

func (c *Config) CreateMail() MailServer {
	return MailServer{
		Domain:     "localhost", // should come from env or cli arguments
		Host:       "localhost",
		Port:       1025,
		Encryption: "none",
		FromName:   "Info",
		FromAddr:   "info@mycompany.com",
		WaitGroup:  c.wg,
		ErrorChan:  make(chan error),
		MailerChan: make(chan Message, 100),
		DoneChan:   make(chan bool),
	}
}
