package main

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func (c *Config) routes() http.Handler {
	mux := chi.NewRouter()

	// setup middlewares
	mux.Use(middleware.Recoverer)
	mux.Use(c.SessionLoad)

	// define app routes
	mux.Get("/", c.HomePage)
	mux.Get("/login", c.LoginPage)
	mux.Post("/loigin", c.PostLoginPage)
	mux.Get("/logout", c.Logout)
	mux.Get("/register", c.RegisterPage)
	mux.Post("/register", c.PostRegisterPage)
	mux.Post("/activate", c.ActivateAccount)

	mux.Mount("/members", c.AuthRouter())

	// Sync test endpoint
	// mux.Get("/test-email", func(w http.ResponseWriter, r *http.Request) {
	// 	m := MailServer{
	// 		Domain:     "localhost",
	// 		Host:       "localhost",
	// 		Port:       1025,
	// 		Encryption: "none",
	// 		FromAddr:   "info@mycompany.com",
	// 		FromName:   "Info",
	// 		ErrorChan:  make(chan error),
	// 	}

	// 	msg := Message{
	// 		ToAddr:  "ms@here.com",
	// 		Subject: "Test email",
	// 		Data:    "Hello world",
	// 	}

	// 	m.SendMail(msg, make(chan error))
	// })

	return mux
}

func (c *Config) AuthRouter() http.Handler {
	mux := chi.NewRouter()
	mux.Use(c.Auth)

	mux.Get("/plans", c.ChooseSubscription)
	mux.Get("/subscribe", c.SubscribeToPlan)

	return mux
}
