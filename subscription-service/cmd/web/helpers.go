package main

func (c *Config) sendEmail(msg Message) {
	c.wg.Add(1)
	c.Mailer.MailerChan <- msg
}
