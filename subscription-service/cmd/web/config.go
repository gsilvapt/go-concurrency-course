package main

import (
	"database/sql"
	"log"
	"subscription-service/data"
	"sync"

	"github.com/alexedwards/scs/v2"
)

type Config struct {
	Session       *scs.SessionManager
	DB            *sql.DB
	InfoLog       *log.Logger
	ErrorLog      *log.Logger
	wg            *sync.WaitGroup
	Models        data.Models
	Mailer        MailServer
	ErrorChan     chan error
	ErrorChanDone chan bool
}
