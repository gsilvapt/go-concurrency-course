package main

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"subscription-service/data"
)

func (c *Config) HomePage(w http.ResponseWriter, r *http.Request) {
	c.render(w, r, "home.page.gohtml", nil)
}

func (c *Config) LoginPage(w http.ResponseWriter, r *http.Request) {
	c.render(w, r, "login.page.gohtml", nil)
}

func (c *Config) PostLoginPage(w http.ResponseWriter, r *http.Request) {
	_ = c.Session.RenewToken(r.Context())

	// parse form post
	if err := r.ParseForm(); err != nil {
		c.ErrorLog.Println(err)
	}

	email := r.Form.Get("email")
	pwd := r.Form.Get("password")

	u, err := c.Models.User.GetByEmail(email)
	if err != nil {
		c.Session.Put(r.Context(), "error", "invalid credentials")
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	match, err := u.PasswordMatches(pwd)
	if err != nil {
		c.Session.Put(r.Context(), "error", "invalid credentials")
		http.Redirect(w, r, "/login", http.StatusBadRequest)
		return
	}

	switch match {
	case false:
		c.sendEmail(Message{
			ToAddr:  email,
			Subject: "Failed log in attempt",
			Data:    "Invalid login attempt",
		})

		c.Session.Put(r.Context(), "error", "invalid credentials")
		http.Redirect(w, r, "/login", http.StatusBadRequest)
	case true:
		c.Session.Put(r.Context(), "userID", u.ID)
		c.Session.Put(r.Context(), "user", u)
		c.Session.Put(r.Context(), "flash", "successful login")
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}

}

func (c *Config) Logout(w http.ResponseWriter, r *http.Request) {
	_ = c.Session.Destroy(r.Context())
	_ = c.Session.RenewToken(r.Context())

	http.Redirect(w, r, "/login", http.StatusSeeOther)
}

func (c *Config) RegisterPage(w http.ResponseWriter, r *http.Request) {
	c.render(w, r, "register.page.gohtml", nil)
}

func (c *Config) PostRegisterPage(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		c.ErrorLog.Println(err)
	}
	// TODO: Validate data, ensuring users does not already exists and alike

	// create a user
	u := data.User{
		Email:     r.Form.Get("email"),
		FirstName: r.Form.Get("first-name"),
		LastName:  r.Form.Get("last-name"),
		Password:  r.Form.Get("password"),
		Active:    0,
	}

	if _, err := u.Insert(u); err != nil {
		c.Session.Put(r.Context(), "error", "unable to create user.")
		http.Redirect(w, r, "/register", http.StatusSeeOther)
		return
	}

	// send an activation email
	url := fmt.Sprintf("http://localhost:8080/activate?email=%s", u.Email) // usually the url gets read from environment variables
	signedUrl := GenerateTokenFromString(url)
	c.InfoLog.Println(signedUrl) // just for demonstration, this shouldn't be there

	msg := Message{
		ToAddr:  u.Email,
		Subject: "Activate your account",
		Templ:   "confirmation-email",
		Data:    template.HTML(signedUrl),
	}
	c.sendEmail(msg)
	c.Session.Put(r.Context(), "flash", "confirmation email sent.")
	http.Redirect(w, r, "/login", http.StatusSeeOther)
}

func (c *Config) ActivateAccount(w http.ResponseWriter, r *http.Request) {
	// validate url
	uri := r.RequestURI
	testURL := fmt.Sprintf("http://localhost:8080%s", uri) // usually the host would come from .env or environment variables

	if okay := VerifyToken(testURL); !okay {
		c.Session.Put(r.Context(), "error", "invalid token")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	// generate invoice
	u, err := c.Models.User.GetByEmail(r.URL.Query().Get("email"))
	if err != nil {
		c.Session.Put(r.Context(), "error", "invalid token")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	u.Active = 1
	if err := u.Update(); err != nil {
		c.Session.Put(r.Context(), "flash", "unexpected back-end error.")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	c.Session.Put(r.Context(), "flash", "account activated, you can now login.")
	http.Redirect(w, r, "/login", http.StatusSeeOther)
}

func (c *Config) ChooseSubscription(w http.ResponseWriter, r *http.Request) {
	plans, err := c.Models.Plan.GetAll()
	if err != nil {
		c.ErrorLog.Println(err)
		return
	}

	dm := make(map[string]any)
	dm["plans"] = plans

	c.render(w, r, "plans.page.gohtml", &TmplData{Data: dm})
}

func (c *Config) SubscribeToPlan(w http.ResponseWriter, r *http.Request) {
	// get the plan ID
	id := r.URL.Query().Get("id")
	planId, _ := strconv.Atoi(id)

	// get the plan from the DB
	plan, err := c.Models.Plan.GetOne(planId)
	if err != nil {
		c.Session.Put(r.Context(), "flash", "unexpected back-end error.")
		http.Redirect(w, r, "/members/plans", http.StatusSeeOther)
		return
	}

	// get the user from the session
	user, ok := c.Session.Get(r.Context(), "user").(data.User)
	if !ok {
		c.Session.Put(r.Context(), "flash", "no valid session")
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	// generate invoice
	c.wg.Add(1)

	go func() {
		defer c.wg.Done()

		invoice, err := c.getInvoice(user, plan)
		if err != nil {
			// send error to channel
			c.ErrorChan <- err
		}

		msg := Message{
			ToAddr:  user.Email,
			Subject: "Your invoice",
			Data:    invoice,
			Templ:   "invoice",
		}

		// send email with invoice attached
		c.sendEmail(msg)

	}()

	// subscribe user to account
	// redirect
	c.Session.Put(r.Context(), "flash", "Subscribed!")
	http.Redirect(w, r, "/members/plans", http.StatusSeeOther)
}

func (c *Config) getInvoice(u data.User, p *data.Plan) (string, error) {
	return p.PlanAmountFormatted, nil
}
