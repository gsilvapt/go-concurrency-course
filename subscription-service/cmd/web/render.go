package main

import (
	"fmt"
	"html/template"
	"net/http"
	"subscription-service/data"
	"time"
)

var pathToTempls = "./cmd/web/templates"

type TmplData struct {
	StringMap     map[string]string
	IntMap        map[string]int
	FloatMap      map[string]float64
	Data          map[string]any
	Flash         string
	Warning       string
	Error         string
	Authenticated bool
	Now           time.Time
	User          *data.User
}

func (c *Config) render(w http.ResponseWriter, r *http.Request, tmpl string, data *TmplData) {
	partials := []string{
		fmt.Sprintf("%s/base.layout.gohtml", pathToTempls),
		fmt.Sprintf("%s/header.partial.gohtml", pathToTempls),
		fmt.Sprintf("%s/navbar.partial.gohtml", pathToTempls),
		fmt.Sprintf("%s/footer.partial.gohtml", pathToTempls),
		fmt.Sprintf("%s/alerts.partial.gohtml", pathToTempls),
	}

	var templSlice []string
	templSlice = append(templSlice, fmt.Sprintf("%s/%s", pathToTempls, tmpl))
	templSlice = append(templSlice, partials...)

	if data == nil {
		data = &TmplData{}
	}

	tmplate, err := template.ParseFiles(templSlice...)
	if err != nil {
		c.ErrorLog.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := tmplate.Execute(w, c.AddDefaultData(data, r)); err != nil {
		c.ErrorLog.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Config) AddDefaultData(td *TmplData, r *http.Request) *TmplData {
	td.Flash = c.Session.PopString(r.Context(), "flash")
	td.Warning = c.Session.PopString(r.Context(), "warning")
	td.Error = c.Session.PopString(r.Context(), "error")
	if c.IsAuthenticated(r) {
		td.Authenticated = true
		user, ok := c.Session.Get(r.Context(), "user").(data.User)
		if !ok {
			c.ErrorLog.Println("can't get user from session")
		} else {
			td.User = &user
		}

	}
	td.Now = time.Now()
	return td
}

func (c *Config) IsAuthenticated(r *http.Request) bool {
	return c.Session.Exists(r.Context(), "userID")
}
