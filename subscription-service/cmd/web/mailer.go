package main

import (
	"bytes"
	"fmt"
	"html/template"
	"sync"
	"time"

	"github.com/vanng822/go-premailer/premailer"
	mail "github.com/xhit/go-simple-mail/v2"
)

type MailServer struct {
	Domain     string
	Host       string
	Port       int
	Username   string
	Password   string
	Encryption string
	FromAddr   string
	FromName   string
	WaitGroup  *sync.WaitGroup
	MailerChan chan Message
	ErrorChan  chan error
	DoneChan   chan bool
}

type Message struct {
	FromAddr    string
	FromName    string
	ToAddr      string
	Subject     string
	Attachments []string
	Data        any
	DataMap     map[string]any
	Templ       string
}

// func to listen for message on the mailerchan
func (c *Config) ListenForMail() {
	for {
		select {
		case msg := <-c.Mailer.MailerChan:
			go c.Mailer.SendMail(msg, c.Mailer.ErrorChan)
		case err := <-c.Mailer.ErrorChan:
			c.ErrorLog.Println(err)
		case <-c.Mailer.DoneChan:
			return
		}
	}
}

func (ms *MailServer) SendMail(msg Message, errChan chan error) {
	defer ms.WaitGroup.Wait()

	if msg.Templ == "" {
		msg.Templ = "mail" // it will catch both html and plaintext templates
	}

	if msg.FromAddr == "" {
		msg.FromAddr = ms.FromAddr
	}

	if msg.FromName == "" {
		msg.FromName = ms.FromName
	}

	msg.DataMap = map[string]any{
		"message": msg.Data, // that's what the template is using as key
	}

	// build html email
	fmtHtmlMsg, err := ms.BuildHtmlMsg(msg)
	if err != nil {
		errChan <- err
	}

	// build plain text email
	fmtPlainMsg, err := ms.BuildPlainMsg(msg)
	if err != nil {
		errChan <- err
	}

	smtpSrv := mail.NewSMTPClient()
	smtpSrv.Host = ms.Host
	smtpSrv.Port = ms.Port
	smtpSrv.Username = ms.Username
	smtpSrv.Password = ms.Password
	smtpSrv.Encryption = ms.GetEncryption(ms.Encryption)
	smtpSrv.KeepAlive = false
	smtpSrv.ConnectTimeout = 10 * time.Second
	smtpSrv.SendTimeout = 10 * time.Second

	cli, err := smtpSrv.Connect()
	if err != nil {
		errChan <- err
	}

	email := mail.NewMSG()
	email.SetFrom(msg.FromAddr).AddTo(msg.ToAddr).SetSubject(msg.Subject)
	email.SetBody(mail.TextPlain, fmtPlainMsg)
	email.AddAlternative(mail.TextHTML, fmtHtmlMsg)

	if len(msg.Attachments) > 0 {
		for _, a := range msg.Attachments {
			email.AddAttachment(a)
		}
	}

	if err := email.Send(cli); err != nil {
		errChan <- err
	}

}

func (ms *MailServer) BuildHtmlMsg(msg Message) (string, error) {
	tmplToRender := fmt.Sprintf("./cmd/web/templates/%s.html.gohtml", msg.Templ)
	t, err := template.New("email-html").ParseFiles(tmplToRender)
	if err != nil {
		return "", err
	}

	var tpl bytes.Buffer
	if err := t.ExecuteTemplate(&tpl, "body", msg.DataMap); err != nil {
		return "", err
	}

	fmtMsg := tpl.String()
	fmtMsg, err = ms.inlineCss(fmtMsg)
	if err != nil {
		return "", err
	}

	return fmtMsg, nil
}

func (ms *MailServer) inlineCss(s string) (string, error) {
	options := premailer.Options{
		RemoveClasses:     false,
		CssToAttributes:   false,
		KeepBangImportant: true,
	}

	prem, err := premailer.NewPremailerFromString(s, &options)
	if err != nil {
		return "", err
	}

	html, err := prem.Transform()
	if err != nil {
		return "", err
	}

	return html, nil
}

func (ms *MailServer) BuildPlainMsg(msg Message) (string, error) {
	tmplToRender := fmt.Sprintf("./cmd/web/templates/%s.plain.gohtml", msg.Templ)
	t, err := template.New("email-plain").ParseFiles(tmplToRender)
	if err != nil {
		return "", err
	}

	var tpl bytes.Buffer
	if err := t.ExecuteTemplate(&tpl, "body", msg.DataMap); err != nil {
		return "", err
	}

	return tpl.String(), nil
}

func (ms *MailServer) GetEncryption(e string) mail.Encryption {
	switch e {
	case "tls":
		return mail.EncryptionSTARTTLS
	case "ssl":
		return mail.EncryptionSSLTLS
	case "none":
		return mail.EncryptionNone
	default:
		return mail.EncryptionSTARTTLS
	}
}
