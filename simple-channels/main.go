package main

import (
	"fmt"
	"strings"
)

func shout(ping <-chan string, pong chan<- string) {
	for {
		s, ok := <-ping
		if !ok {
			// do something
		}
		pong <- fmt.Sprintf("%s!!!", strings.ToUpper(s))
	}
}

func main() {
	ping, pong := make(chan string), make(chan string)
	go shout(ping, pong)

	fmt.Println("Type something and press ENTER (enter Q to quit)")
	for {
		fmt.Print("-> ")
		var input string
		_, _ = fmt.Scanln(&input)

		if strings.ToLower(input) == "q" {
			break
		}

		ping <- input
		resp := <-pong
		fmt.Println("Response: ", resp)
	}

	fmt.Println("All done. closing channels")
	close(ping)
	close(pong)
}
