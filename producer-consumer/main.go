package main

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/fatih/color"
)

const NumberOfPizzas = 10

var pizzasMade, pizzasFailed, total int

type PizzaOrder struct {
	number  int
	msg     string
	success bool
}

type Producer struct {
	data chan PizzaOrder
	quit chan chan error
}

func (p *Producer) Close() error {
	ch := make(chan error)
	p.quit <- ch
	return <-ch // return any error sent to the channel
}

type Consumer struct {
}

func makePizza(number int) *PizzaOrder {
	number++
	if number > NumberOfPizzas {
		return &PizzaOrder{
			number: number,
		}
	}

	delay := rand.Intn(5) + 1
	fmt.Printf("received order number #%d.\n", number)

	rnd := rand.Intn(12) + 1
	msg := ""
	success := false

	if rnd < 5 {
		pizzasFailed++
	} else {
		pizzasMade++
	}
	total++

	fmt.Printf("making pizza number #%d. It will take %d seconds...\n", number, delay)
	time.Sleep(time.Duration(delay) * time.Second)

	if rnd <= 2 {
		msg = fmt.Sprintf("*** we ran out of ingredients for pizza #%d! ***\n", number)
	} else if rnd <= 4 {
		msg = fmt.Sprintf("*** the cook quit while making pizza #%d! ***\n", number)
	} else {
		success = true
		msg = fmt.Sprintf("pizza order #%d is ready!\n", number)
	}

	return &PizzaOrder{
		number:  number,
		msg:     msg,
		success: success,
	}

}

func pizzeria(maker *Producer) {
	var i int = 0
	// keep track of which pizza we're making
	// run forever or until we receivee a quit notification
	// try to make pizzas
	for {
		current := makePizza(i)
		if current != nil {
			i = current.number
			select {
			// we try to make a pizza
			case maker.data <- *current:
			case quitChan := <-maker.quit:
				close(maker.data)
				close(quitChan)
				return
			}
			// try to make pizza
			// decision structure after making pizza
		}
	}
}

func main() {
	// Seed random number generator
	rand.Seed(time.Now().UnixNano())
	// Print out message about program starting
	color.Cyan("The Pizzeria is open for business")
	color.Cyan("---------------------------------")

	// Create a Producer
	job := &Producer{
		data: make(chan PizzaOrder),
		quit: make(chan chan error),
	}
	// Run the producer in the background
	go pizzeria(job)

	// Create and run Consumer(s)
	for i := range job.data {
		if i.number <= NumberOfPizzas {
			if i.success {
				color.Green(i.msg)
				color.Green("order #%d is out for delivery", i.number)
			} else {
				color.Red(i.msg)
				color.Red("the customer is really mad!")
			}
		} else {
			color.Cyan("done making pizzas")
			err := job.Close()
			if err != nil {
				color.Red("*** error closing channel!", err)
			}
		}
	}
	// Print out the ending message
	color.Cyan("----------------")
	color.Cyan("done for the day")

	color.Cyan("we made %d pizzas, but failed to make %d, with %d attempts in total", pizzasMade, pizzasFailed, total)
	switch {
	case pizzasFailed > 9:
		color.Red("it was an awful day")
	case pizzasFailed >= 6:
		color.Red("it was not a good day...")
	case pizzasFailed >= 4:
		color.Yellow("it was an ok day...")
	case pizzasFailed >= 2:
		color.Yellow("it was a pretty good day!")
	default:
		color.Green("it was a great day!")
	}
}
