package main

import (
	"io"
	"os"
	"strings"
	"sync"
	"testing"
)

func Test_printSomethin(t *testing.T) {
	stdout := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	var wg sync.WaitGroup
	wg.Add(1)

	go printSomething("epsilon", &wg)

	wg.Wait()

	_ = w.Close() // close pipe

	result, _ := io.ReadAll(r)
	output := string(result)

	os.Stdout = stdout

	if !strings.Contains(output, "epsilon") {
		t.Errorf("expected to find epsilong but it is %s", output)
	}
}
