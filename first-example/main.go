package main

import (
	"fmt"
	"sync"
)

func printSomething(s string, wg *sync.WaitGroup) {
	defer wg.Done()
	fmt.Println(s)
}

func main() {
	// go printSomething("this is something to be printed")

	// printSomething("this is the second thing to be printed")

	// the above code essentially would not print both lines, just the latter, because of the execution speed.
	// Adding a time.Sleep call would be dump. So... Enter WaitGroups:
	var wg sync.WaitGroup

	words := []string{
		"alpha",
		"beta",
		"delta",
		"gamma",
		"pi",
		"zeta",
		"eta",
		"theta",
		"epsilon",
	}

	wg.Add(9) // instead of hardcoding, we could do wg.Add(len(words))
	for i, w := range words {
		go printSomething(fmt.Sprintf("%d: %s", i, w), &wg)
	}
	wg.Wait()

	wg.Add(1)
	printSomething("this is the second thing to be printed", &wg)
}
