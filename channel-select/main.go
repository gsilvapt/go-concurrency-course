package main

import (
	"fmt"
	"time"
)

func server1(ch chan string) {
	for {
		time.Sleep(6 * time.Second)
		ch <- "this is from server 1"
	}
}

func server2(ch chan string) {
	for {
		time.Sleep(3 * time.Second)
		ch <- "this is from server 2"
	}
}

func main() {
	fmt.Println("Select with channels")
	fmt.Println("--------------------")

	ch1, ch2 := make(chan string), make(chan string)

	go server1(ch1)
	go server2(ch2)

	for {
		select {
		case s1 := <-ch1:
			fmt.Println("Case one: ", s1)
		case s2 := <-ch1:
			fmt.Println("Case two: ", s2)
		case s3 := <-ch2:
			fmt.Println("Case three: ", s3)
		case s4 := <-ch2:
			fmt.Println("Case four: ", s4)
			// default:
			// avoiding deadlocks, even though if it's empty.
			// but when running under a for loop, it's irrelevant.
		}
	}
	// select statements do not stop when the first condition is met.
	// When multiple cases match the same condition, it is chosen random.
	// In some situations this can be useful.
}
