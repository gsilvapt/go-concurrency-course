package main

import (
	"fmt"
	"time"
)

func chanListen(ch chan int) {
	for {
		i := <-ch
		fmt.Printf("Got %d from channel \n", i)

		// doing a lot of work.
		time.Sleep(1 * time.Second)
	}

}

func main() {
	ch := make(chan int, 10)
	go chanListen(ch)

	for i := 0; i <= 100; i++ {
		fmt.Println("Sending", i, "to channel")
		ch <- i
		fmt.Println("Sent", i, "to channel!")
	}
	fmt.Println("Done!")
	close(ch)
}
