package main

import (
	"io"
	"os"
	"strings"
	"testing"
)

func Test_updateMsg(t *testing.T) {
	wg.Add(1)

	go updateMsg("epsilon")
	wg.Wait()

	if msg != "epsilon" {
		t.Errorf("Expected epsilon but got %s", msg)
	}
}

func Test_printMsg(t *testing.T) {
	stdout := os.Stdout
	r, w, _ := os.Pipe()

	os.Stdout = w

	msg = "Epsilon"
	printMsg()
	_ = w.Close()

	reader, _ := io.ReadAll(r)
	result := string(reader)
	os.Stdout = stdout

	if !strings.Contains(result, "Epsilon") {
		t.Errorf("Expected to find epsilong, got %s", result)
	}
}

func Test_main(t *testing.T) {
	stdout := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	main()

	_ = w.Close()

	reader, _ := io.ReadAll(r)
	result := string(reader)
	os.Stdout = stdout

	expects := []string{
		"Hello, universe!",
		"Hello, cosmos!",
		"Hello, world!",
	}

	for _, e := range expects {
		if !strings.Contains(result, e) {
			t.Errorf("Expected %s in %s", e, result)
		}
	}
}
