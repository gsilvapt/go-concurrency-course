package main

import (
	"fmt"
	"sync"
)

var (
	msg string
	wg  sync.WaitGroup
)

func updateMsg(s string) {
	defer wg.Done()
	msg = s
}

func printMsg() {
	fmt.Println(msg)
}

func main() {
	msg = "Hello, world!"

	wg.Add(1)
	go updateMsg("Hello, universe!")
	wg.Wait()
	printMsg()

	wg.Add(1)
	go updateMsg("Hello, cosmos!")
	wg.Wait()
	printMsg()

	wg.Add(1)
	go updateMsg("Hello, world!")
	wg.Wait()
	printMsg()
}
